import HFlow from './HFlow';

const sleep = require('system-sleep')
const parseArgs = require('minimist')(process.argv.slice(2));
const path = parseArgs.path;

console.log('[INF] Loading' + path);

if (path != null) {
  let flow = new HFlow(path);
  console.log(flow);
} else {
    console.log('No flow file found.')
}

let done = false;

while (!done) {
  sleep(1000);
}
