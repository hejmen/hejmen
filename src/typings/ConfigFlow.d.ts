type ConfigFlow = {
    blocks: ConfigBlock[] | null;
    modules: ConfigModule[] | null;
    connections: ConfigConnection[] | null;
}
