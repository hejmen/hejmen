import HModule from '../../HModule';
import HBlock from '../../HBlock';

class ConsoleLogBlock extends HBlock {
  static NAME = 'console';

  constructor(module: HModule) {
    super(module, ConsoleLogBlock.NAME);
    this.mInPipes = ['in'];
  }

  process(pipe: string, data: any): boolean {
    if (super.process(pipe, data)) {
      console.log(data);
      return true;
    }
    return false;
  }
}

export default class LogHModule extends HModule {
  getVersion(): TVersion {
    return {minor: 0, major: 0, patch: 1};
  }

  getBlockTypes(): string[] {
    return [ConsoleLogBlock.NAME];
  }

  createBlock(type: string): HBlock | null {
    if (type === ConsoleLogBlock.NAME) {
      return new ConsoleLogBlock(this);
    }
    return null;
  }
}
