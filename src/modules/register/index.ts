import HModule from '../../HModule';
import HBlock from '../../HBlock';

class RegisterBlock extends HBlock {
  static NAME = 'register';
  private mData:any;

  constructor(module: HModule) {
    super(module, RegisterBlock.NAME);
    this.mOutPipes = ['out'];
    this.mInPipes = ['in', 'trigger'];
  }

  process(pipe: string, data: any): boolean {
    if (super.process(pipe, data)) {
      if (pipe === 'in') {
        this.mData = data;
      } else if (pipe === 'trigger') {
        this.send('out', this.mData);
      }
      return true;
    }
    return false;
  }
}

export default class RegisterHModule extends HModule {
  getVersion(): TVersion {
    return {minor: 0, major: 0, patch: 1};
  }

  getBlockTypes(): string[] {
    return [RegisterBlock.NAME];
  }

  createBlock(type: string): HBlock | null {
    if (type === RegisterBlock.NAME) {
      return new RegisterBlock(this);
    }
    return null;
  }
}
