type ConfigModule = {
    id: string;
    module: string;
    version: TVersion;
}
