import HModule from '../../HModule';
import HBlock from '../../HBlock';

class TimerBlock extends HBlock {
  static NAME = 'timer';

  interval: any;

  constructor(module: HModule) {
    super(module, TimerBlock.NAME);
    this.mOutPipes = ['tick'];
  }

  enabled(): void {
    // Validate
    if (this.mParams.time > 0) {
      this.interval = setInterval(() => {
        this.send('tick');
      }, this.mParams.time);
    }
  }

  disabled(): void {
    clearInterval(this.interval);
  }
}

export default class TimerHModule extends HModule {
  getVersion(): TVersion {
    return {minor: 0, major: 0, patch: 1};
  }

  getBlockTypes(): string[] {
    return [TimerBlock.NAME];
  }

  createBlock(type: string): HBlock | null {
    if (type === TimerBlock.NAME) {
      return new TimerBlock(this);
    }
    return null;
  }
}
