import HModule from '../../HModule';
import HBlock from '../../HBlock';
const { exec } = require('child_process');
const Mustache = require('mustache');

class ExecBlock extends HBlock {
  static NAME = 'exec';
  private deamon: any;

  constructor(module: HModule) {
    super(module, ExecBlock.NAME);
    this.mOutPipes = ['out', 'err'];
    this.mInPipes = ['in'];
  }

  process(pipe: string, data: any): boolean {
    if (super.process(pipe, data)) {
      exec(Mustache.render(this.mParams.cmd, data), (error, stdout, stderr) => {
        this.send('err', stderr);
        this.send('out', stdout);
      });
      return true;
    }
    return false;
  }

  enabled(): void {
    if (this.mParams.deamon) {
      console.log('')
      this.deamon = exec(this.mParams.cmd);
      this.deamon.stdout.on('data', (data) => {
        this.send('out', data);
      });
      this.deamon.stderr.on('data', (data) => {
        this.send('err', data);
      });
    }
  }

  disabled(): void {
    if (this.mParams.deamon) {
      this.deamon.kill('SIGHUP');
    }
  }
}

export default class ExecHModule extends HModule {
  getVersion(): TVersion {
    return {minor: 0, major: 0, patch: 1};
  }

  getBlockTypes(): string[] {
    return [ExecBlock.NAME];
  }

  createBlock(type: string): HBlock | null {
    if (type === ExecBlock.NAME) {
      return new ExecBlock(this);
    }
    return null;
  }
}
