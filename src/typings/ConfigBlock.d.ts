type ConfigBlock = {
    id: number;
    module: string;
    type: string;
    parameters: any;
}
