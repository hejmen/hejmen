import ModuleManager from './ModuleManager';
import HBlock from './HBlock';
const fs = require('fs');
const path = require('path');

export default class HFlow {
  blocks: { [id: string]: HBlock; } = {};

  constructor(filename: string) {
    // Load the flow
    const filepath = path.resolve(__dirname, filename);
    const obj: ConfigFlow = JSON.parse(fs.readFileSync(filepath, 'utf8'));
    this.load(obj);
  }

  load(config: ConfigFlow): void {
    // For each module
    if (config.modules) {
      for (let i = 0; i < config.modules.length; i++) {
        const module: ConfigModule = config.modules[i];
        this.loadModule(module);
      }
    }

    // For each block
    if (config.blocks) {
      for (let i = 0; i < config.blocks.length; i++) {
        const block: ConfigBlock = config.blocks[i];
        this.loadBlock(block);
      }
    }

    // For each connection
    if (config.connections) {
      for (let i = 0; i < config.connections.length; i++) {
        const conn: ConfigConnection = config.connections[i];
        const ib = this.blocks[conn.in.block];
        const ob = this.blocks[conn.out.block];
        ib.addPipe(conn.in.pipe, ob, conn.out.pipe);
      }
    }

    // Enable each block
    for (const b of Object.keys(this.blocks)) {
      this.blocks[b].enable();
    }
  }

  loadModule(module: ConfigModule): void {
    console.log('[INF] Loading module: ' + module.id);
    const mod = ModuleManager.loadModule(module.id);
    // TODO: check version
    console.log(mod.getVersion());
  }

  loadBlock(block: ConfigBlock): void {
    console.log(block);
    // Create block
    const mod = ModuleManager.getModule(block.module);
    let b = mod.createBlock(block.type);
    console.log(b);
    if (b != null) {
      console.log(this.blocks);
      this.blocks[block.id] = b;
      b.setParameters(block.parameters);
      // b.addPipe('out', );
      b.ready();
    }
  }
}
