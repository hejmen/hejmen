import HModule from '../../HModule';
import HBlock from '../../HBlock';

class RegExBlock extends HBlock {
  static NAME = 'regex';
  private regex;

  constructor(module: HModule) {
    super(module, RegExBlock.NAME);
    this.mOutPipes = ['out'];
    this.mInPipes = ['in'];
  }

  ready(): void {
    this.regex = new RegExp(this.mParams.regex, this.mParams.flags);
  }

  process(pipe: string, data: any): boolean {
    if (super.process(pipe, data)) {
      if (typeof data === 'string') {
        if (this.mParams.type === "exec") {
          const result = this.regex.exec(data);
          if (result) {
            this.send('out', result);
          }
        }
        else if (this.mParams.type === "match") {
          const result = data.match(this.regex);
          if (result) {
            this.send('out', result);
          }
        } else {
          console.log("Wrong type of regex");
        }
      }
      return true;
    }
    return false;
  }
}

export default class RegExHModule extends HModule {
  getVersion(): TVersion {
    return {minor: 0, major: 0, patch: 1};
  }

  getBlockTypes(): string[] {
    return [RegExBlock.NAME];
  }

  createBlock(type: string): HBlock | null {
    if (type === RegExBlock.NAME) {
      return new RegExBlock(this);
    }
    return null;
  }
}
