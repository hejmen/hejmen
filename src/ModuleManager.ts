// const fs = require('fs');
// const path = require('path');

import HModule from './HModule';

// Global
let modules: { [id: string]: HModule; } = {}

// Modules loader
/*
fs.readdir(path.resolve(__dirname, 'modules'), (err, items) => {
  items.forEach((item) => {
    const module = new (require('./modules/' + item + '/index.js').default)

    // Each module is instance of HModule
    if (module instanceof HModule) {
      console.log('[INF] Module loaded: ' + item)
      modules[item] = module;
    } else {
      console.log('[ERR] Module err: ' + item)
    }
  })
  events.emit('ModuleManagerReady')
})
*/

namespace ModuleManager {
  export function getModules() {
    return modules;
  }

  export function getModule(name: string): HModule {
    return modules[name];
  }

  export function loadModule(name: string): HModule {
    const module = new (require('./modules/' + name + '/index.js').default)
    modules[name] = module;
    return module;
  }
}

export default ModuleManager;
