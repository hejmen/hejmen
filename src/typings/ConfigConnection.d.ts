type Node = {
  block: number;
  pipe: string;
}

type ConfigConnection = {
  in: Node;
  out: Node;
}
