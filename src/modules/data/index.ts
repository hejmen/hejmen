import HModule from '../../HModule';
import HBlock from '../../HBlock';

class DataBlock extends HBlock {
  static NAME = 'data';

  constructor(module: HModule) {
    super(module, DataBlock.NAME);
    this.mOutPipes = ['out'];
    this.mInPipes = ['in'];
  }

  process(pipe: string, data: any): boolean {
    if (super.process(pipe, data)) {
      this.send('out', this.mParams.data);
      return true;
    }
    return false;
  }
}

export default class DataHModule extends HModule {
  getVersion(): TVersion {
    return {minor: 0, major: 0, patch: 1};
  }

  getBlockTypes(): string[] {
    return [DataBlock.NAME];
  }

  createBlock(type: string): HBlock | null {
    if (type === DataBlock.NAME) {
      return new DataBlock(this);
    }
    return null;
  }
}
