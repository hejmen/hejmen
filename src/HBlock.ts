import HModule from './HModule'

export default abstract class HBlock implements Block {
  protected mInPipes: string[] = [];
  protected mOutPipes: string[] = [];

  protected mModule: HModule;
  protected mParams: any = {}

  mPipes: {[id: string]: InPipe[]} = {};
  mEnabled = false;
  mName: string;

  constructor(module: HModule, name: string) {
    this.mModule = module;
    this.mName = name;
  }

  ready(): void {}

  getName(): string {
    return this.mName;
  }

  getOutPipeNames(): string[] {
    return this.mOutPipes;
  }

  getInPipeNames(): string[] {
    return this.mInPipes;
  }

  getHModule(): HModule {
    return this.mModule;
  }

  getParameters(): any {
    return this.mParams;
  }
  setParameters(params: any): void {
    this.mParams = params;
  }

  isEnabled(): boolean {
    return this.mEnabled;
  }

  protected enabled(): void {}
  protected disabled(): void {}

  enable(): void {
    if (this.mEnabled) {
      return;
    }
    this.mEnabled = true;
    this.enabled();
  }

  disable(): void {
    if (!this.mEnabled) {
      return;
    }
    this.mEnabled = false;
    this.disabled();
  }

  // Empty processor
  process(pipe: string, data: any): boolean {
    if (this.getInPipeNames().indexOf(pipe) > -1) {
      return true;
    }
    return false;
  }

  protected send(pipe: string, data: any = null): void {
    if (this.mPipes[pipe] != null) {
      const inPipeArr = this.mPipes[pipe];
      for (let i = 0; i < inPipeArr.length; i++) {
        const inPipe: InPipe = inPipeArr[i];
        const b: HBlock = <HBlock>inPipe.block;
        b.process(inPipe.pipe, data);
      }
    }
  }

  getOutPipes(): {[id: string]: InPipe[]} {
    return this.mPipes;
  }

  addPipe(id: string, block: HBlock, pipe: string): boolean {
    if (this.getOutPipeNames().indexOf(id) > -1) {
      if (this.mPipes[id] == null) {
        this.mPipes[id] = [];
      }
      const p: InPipe = {block: block, pipe: pipe};
      this.mPipes[id].push(p);
      return true;
    }
    return false;
  }

  removePipe(id: string, block: HBlock, pipe: string | null = null): boolean {
    if (typeof this.mPipes[id] == null) {
      return false;
    }
    for (let i = 0; i < this.mPipes[id].length; i++) {
      if (this.mPipes[id][i].block === block &&
           (pipe == null || this.mPipes[id][i].pipe === pipe)) {
        this.mPipes[id].splice(i, 1);
        return true;
      }
    }
    return false;
  }
}
