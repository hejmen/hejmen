import HModule from '../../HModule';
import HBlock from '../../HBlock';

class SumBlock extends HBlock {
  static NAME = 'sum';
  private mSum: number = 0;

  constructor(module: HModule) {
    super(module, SumBlock.NAME);
    this.mOutPipes = ['out'];
    this.mInPipes = ['in', 'reset'];
  }

  process(pipe: string, data: any): boolean {
    if (super.process(pipe, data)) {
      if (pipe === 'in') {
        this.mSum += data;
        this.send('out', this.mSum);
      } else if (pipe === 'reset') {
        this.mSum = 0;
      }
      return true;
    }
    return false;
  }
}

class AvgBlock extends HBlock {
  static NAME = 'avg';
  private mSum: number = 0;
  private mCount: number = 0;
  constructor(module: HModule) {
    super(module, AvgBlock.NAME);
    this.mOutPipes = ['out'];
    this.mInPipes = ['in', 'reset'];
  }

  process(pipe: string, data: any): boolean {
    if (super.process(pipe, data)) {
      if (pipe === 'in') {
        this.mCount++;
        this.mSum += data;
        this.send('out', this.mSum / this.mCount);
      } else if (pipe === 'reset') {
        this.mCount = 0;
        this.mSum = 0;
      }
      return true;
    }
    return false;
  }
}

export default class MathHModule extends HModule {
  getVersion(): TVersion {
    return {minor: 0, major: 0, patch: 1};
  }

  getBlockTypes(): string[] {
    return [SumBlock.NAME, AvgBlock.NAME];
  }

  createBlock(type: string): HBlock | null {
		if (type === SumBlock.NAME) {
      return new SumBlock(this);
    } else if (type === AvgBlock.NAME) {
      return new AvgBlock(this);
    }
    return null;
  }
}
