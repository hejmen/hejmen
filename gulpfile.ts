'use strict';

const gulp = require('gulp'),
    del = require('del'),
    tsc = require('gulp-typescript'),
    sourcemaps = require('gulp-sourcemaps'),
    tsProject = tsc.createProject('tsconfig.json'),
    tslint = require('gulp-tslint'),
    concat = require('gulp-concat'),
    nodemon = require('gulp-nodemon');

gulp.task('clean', (cb) => {
    return del(['dist'], cb);
});

gulp.task('tslint', () => {
    return gulp.src(['**/*.ts'])
    .pipe(tslint({
      formatter: 'stylish',
      configuration: 'tslint.json'
    }))
    .pipe(tslint.report());
});

gulp.task('build', function () {
    const tsResult = gulp.src('src/**/*.ts')
        .pipe(sourcemaps.init())
        .pipe(tsProject());
    return tsResult.js
        .pipe(sourcemaps.write())
        .pipe(gulp.dest('dist'));
});

gulp.task('start', function () {
    nodemon({
        script: 'dist/server.js'
        , ext: 'html js'
        , ignore: ['ignored.js']
        , tasks: ['tslint']
    })
        .on('restart', function () {
            console.log('restarted!');
        });
});

gulp.task('watch', function () {
    gulp.watch(['**/*.ts'], ['build']).on('change', function (e) {
        console.log('TypeScript file ' + e.path + ' has been changed. Compiling.');
    });
});
