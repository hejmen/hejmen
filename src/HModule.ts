import HBlock from './HBlock'

export default abstract class HModule {
    abstract getVersion(): TVersion;
    abstract getBlockTypes(): string[];
    abstract createBlock(type: string): HBlock | null;
}
